package demo.vishal.com.jwelarydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private ImageView image;
    private TextView name, price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        image = findViewById(R.id.image);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);

        name.setText(getIntent().getExtras().getString("key_name"));
        price.setText(getIntent().getExtras().getString("key_price"));
        image.setImageResource(getIntent().getExtras().getInt("key_image"));
    }
}
